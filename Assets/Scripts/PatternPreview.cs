﻿/*
Firefly Beacon - Attracts firefly insects to your phone or tablet
Copyright (C) 2016 Keith Simmons <ksimmons@gmail.com>

This file is part of Firefly Beacon.

Firefly Beacon is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firefly Beacon is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firefly Beacon.  If not, see <http://www.gnu.org/licenses/>.

Latest code is available here:
https://bitbucket.org/oneironaut/firefly-beacon
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PatternPreview : MonoBehaviour {
    public Image previewBlinkPrefab;
    public Sprite waitColorSprite;

    BlinkPattern currentPattern;
    
    List<Image> previewBlinks;

    Image playhead;
    double playheadDuration;

    Image waitPeriod;
    double waitTime = 3;

    // Use this for initialization
    void Awake () { // awake so it is subscribed b4 dropdown update
        FireflyBeaconDropdown.PatternChange += OnPatternChanged;
        WaitSelector.WaitTimeChange += OnWaitTimeChanged;

        previewBlinks = new List<Image>();

        // initiate wait period block
        waitPeriod = (Image)Instantiate(previewBlinkPrefab);
        waitPeriod.transform.SetParent(this.transform, false);

        waitPeriod.sprite = waitColorSprite;
        waitPeriod.color = Color.white;
        waitPeriod.type = Image.Type.Filled;
        waitPeriod.fillMethod = Image.FillMethod.Horizontal;
        waitPeriod.fillOrigin = (int)Image.OriginHorizontal.Right;
        waitPeriod.fillAmount = 1.0f;

        // initiate playhead block
        playhead = (Image)Instantiate(previewBlinkPrefab);
        playhead.transform.SetParent(this.transform, false);
        playhead.color = Color.white;
    }

    // Update is called once per frame
    void Update () {
        double totalDisplayTime = currentPattern.timelineLength + waitTime;
        double rectWidth = gameObject.GetComponent<Image>().rectTransform.rect.width;

        double timelinePercentage = (Time.time % totalDisplayTime) / totalDisplayTime;
        double timelinePosition = timelinePercentage * rectWidth;

        // move playhead
        playhead.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, (float)timelinePosition, (float)playheadDuration);

        // this block will show the wait period degrade over time
        /*
        if (timelinePercentage * totalDisplayTime > currentPattern.timelineLength)
        {
            Debug.Log(((totalDisplayTime - timelinePercentage * totalDisplayTime) / waitTime));
            waitPeriod.fillAmount = (float)((totalDisplayTime - timelinePercentage*totalDisplayTime) / waitTime);
            //waitPeriod.enabled = waitPeriod.fillAmount > 0;
        } else
        {
            waitPeriod.fillAmount = 1f;
        }
        */
    }

    void OnWaitTimeChanged(double _waitTime)
    {
        waitTime = _waitTime;
        UpdateView();
    }

    void OnPatternChanged(BlinkPattern pattern)
    {
        currentPattern = pattern;
        UpdateView();
    }

    void UpdateView()
    {
        if (currentPattern == null)
            return;
        
        for (int i = 0; i < previewBlinks.Count; i++)
            Destroy(previewBlinks[i]);

        previewBlinks.Clear();
        
        double totalDisplayTime = currentPattern.timelineLength + waitTime;
        double rectWidth = gameObject.GetComponent<Image>().rectTransform.rect.width;

        // set playhead size
        playheadDuration = 0.05 / totalDisplayTime * rectWidth;


        // show blinks
        for (int i = 0; i < currentPattern.blinks.Count; i++)
        {
            Image tempBlink = (Image)Instantiate(previewBlinkPrefab);
            tempBlink.transform.SetParent(this.transform, false);

            double timelinePosition = (currentPattern.blinks[i].startTime / totalDisplayTime) * rectWidth;
            double timelineDuration = (currentPattern.blinks[i].duration / totalDisplayTime) * rectWidth;
            tempBlink.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, (float)timelinePosition, (float)timelineDuration);
            previewBlinks.Add(tempBlink);
        }

        // show wait
        double waitTimelinePosition = (currentPattern.timelineLength / totalDisplayTime) * rectWidth;
        double waitTimelineDuration = (waitTime / totalDisplayTime) * rectWidth;

        waitPeriod.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, (float)waitTimelinePosition, (float)waitTimelineDuration);
       
        // put playhead back on top
        playhead.transform.SetParent(null, false);
        playhead.transform.SetParent(this.transform, false);
    }
}
