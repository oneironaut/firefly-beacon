﻿/*
Firefly Beacon - Attracts firefly insects to your phone or tablet
Copyright (C) 2016 Keith Simmons <ksimmons@gmail.com>

This file is part of Firefly Beacon.

Firefly Beacon is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firefly Beacon is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firefly Beacon.  If not, see <http://www.gnu.org/licenses/>.

Latest code is available here:
https://bitbucket.org/oneironaut/firefly-beacon
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class FireflyBeaconDropdown: MonoBehaviour {
    public delegate void FireflyBeaconDropdownDelegate(BlinkPattern pattern);
    public static event FireflyBeaconDropdownDelegate PatternChange;

    public TextAsset blinkPatterJsonFile;
    
    BlinkPatternCollection blinkPatterns;
    int patternIndex = 0;

    Dropdown dropdown;

    public void OnDropdownChanged(int id)  // invoke PatternChange
    {
        int count = 0;
        // check dropdown name against pattern names to be sure
        foreach (BlinkPattern pattern in blinkPatterns.patterns)
        {
            if (dropdown.options[id].text == pattern.GetFriendlyName())
            {
                patternIndex = count;
            }
            count++;
        }

        if (PatternChange != null)
            PatternChange.Invoke(blinkPatterns.patterns[patternIndex]);
    }

    // Use this for initialization
    void Start () {
        blinkPatterns = BlinkPatternCollection.CreateFromJSON(blinkPatterJsonFile.text);
        dropdown = gameObject.GetComponent<Dropdown>();

        PopulateDropdown();

        OnDropdownChanged(dropdown.value); // send initial message
	}
	
    void PopulateDropdown()
    {
        // populate dropdown and set to current exhibit
        dropdown.options.Clear();

        foreach (BlinkPattern pattern in blinkPatterns.patterns)
        {
            dropdown.options.Add(new Dropdown.OptionData(pattern.GetFriendlyName()));
        }

        // http://forum.unity3d.com/threads/new-dropdown-options-usage.343097/#post-2223608
        dropdown.captionText = dropdown.captionText; // force update of dropdown caption

    }
    
}
