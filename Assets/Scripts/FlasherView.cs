﻿/*
Firefly Beacon - Attracts firefly insects to your phone or tablet
Copyright (C) 2016 Keith Simmons <ksimmons@gmail.com>

This file is part of Firefly Beacon.

Firefly Beacon is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firefly Beacon is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firefly Beacon.  If not, see <http://www.gnu.org/licenses/>.

Latest code is available here:
https://bitbucket.org/oneironaut/firefly-beacon
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class FlasherView : MonoBehaviour {
    double time;

    bool isMuted = false;

    BlinkPattern currentPattern;

    Image flasherBox;

    double waitTime = 3;
       
    List<double> tempStartTimes = new List<double>();
    List<double> localStartTimes = new List<double>();
    List<double> localDurations = new List<double>();

    public void toggleMute()
    {
        isMuted = !isMuted;
    }

    public void mute()
    {
        isMuted = true;
    }

    public void unmute()
    {
        isMuted = false;
    }

    // Use this for initialization
    void Awake () { // awake so it is subscribed before dropdown inits
        flasherBox = gameObject.GetComponent<Image>();
        FireflyBeaconDropdown.PatternChange += OnPatternChanged;
        WaitSelector.WaitTimeChange += OnWaitTimeChanged;
        time = Time.time;
    }

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    // Update is called once per frame
    void Update () {

        if (!isMuted)
        {
            time = Time.time;

            double totalDisplayTime = currentPattern.timelineLength + waitTime;

            double timelinePosition = time % totalDisplayTime;



            tempStartTimes.Clear();

            // locally copy blinks into arrays
            tempStartTimes = new List<double>(localStartTimes);

            tempStartTimes.Add(timelinePosition);

            tempStartTimes.Sort(); // sort before discovering index

            int currentIndex = tempStartTimes.LastIndexOf(timelinePosition) - 1; // find current time and check where we are in start times
            if (currentIndex < 0)
                currentIndex = 0;

            if (timelinePosition > localStartTimes[currentIndex] && timelinePosition < localStartTimes[currentIndex] + localDurations[currentIndex])
            {
                flasherBox.color = Color.green;
            }
            else
                flasherBox.color = Color.black;

            // add start 
        } else
        {
            flasherBox.color = Color.black;
        }
    }

    void OnWaitTimeChanged(double _waitTime)
    {
        waitTime = _waitTime;
    }

    void OnPatternChanged(BlinkPattern pattern)
    {
        currentPattern = pattern;

        localStartTimes.Clear();
        localDurations.Clear();

        // locally copy blinks into arrays
        localStartTimes = new List<double>(pattern.GetStartTimeList());
        localDurations = new List<double>(pattern.GetDurationList());
    }
}
