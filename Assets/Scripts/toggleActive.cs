﻿/*
Firefly Beacon - Attracts firefly insects to your phone or tablet
Copyright (C) 2016 Keith Simmons <ksimmons@gmail.com>

This file is part of Firefly Beacon.

Firefly Beacon is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firefly Beacon is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firefly Beacon.  If not, see <http://www.gnu.org/licenses/>.

Latest code is available here:
https://bitbucket.org/oneironaut/firefly-beacon
*/

using UnityEngine;
using System.Collections;

public class toggleActive : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ToggleActiveState()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
