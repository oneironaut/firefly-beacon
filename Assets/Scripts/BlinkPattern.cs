﻿/*
Firefly Beacon - Attracts firefly insects to your phone or tablet
Copyright (C) 2016 Keith Simmons <ksimmons@gmail.com>

This file is part of Firefly Beacon.

Firefly Beacon is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firefly Beacon is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firefly Beacon.  If not, see <http://www.gnu.org/licenses/>.

Latest code is available here:
https://bitbucket.org/oneironaut/firefly-beacon
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class BlinkPatternCollection
{
    public List<BlinkPattern> patterns;

    public BlinkPatternCollection()
    {
        patterns = new List<BlinkPattern>();
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this, true);
    }

    public static BlinkPatternCollection CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<BlinkPatternCollection>(jsonString);
    }
}

[System.Serializable]
public class Blink
{
    public double startTime;
    public double duration;

    public Blink(double start, double length)
    {
        startTime = start;
        duration = length;
    }
}


[System.Serializable]
public class BlinkPattern {
    public string genus;
    public string species;
    public string sex;
    public Color color;
    public double timelineLength;
    public List<Blink> blinks;

    bool isInitialized;

    public BlinkPattern()
    {
        species = "hi";
       
        blinks = new List<Blink>();

        blinks.Add(new Blink(1,2.01));
        blinks.Add(new Blink(2, 3));
        blinks.Add(new Blink(3.1, 0.5));
    }

    public List<double> GetStartTimeList()
    {
        List<double> returnList = new List<double>();

        foreach (Blink blink in blinks)
        {
            returnList.Add(blink.startTime);
        }

        return returnList;
    }

    public List<double> GetDurationList()
    {
        List<double> returnList = new List<double>();

        foreach (Blink blink in blinks)
        {
            returnList.Add(blink.duration);
        }

        return returnList;

    }

    public string GetFriendlyName()
    {
        return "P. " + species + " (" + sex + ")";
    }

    public static BlinkPattern CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<BlinkPattern>(jsonString);
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this, true);
    }

}
