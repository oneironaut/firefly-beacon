﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaitSelector : MonoBehaviour {

    Dropdown dropdown;

    public delegate void WaitSelectorDelegate(double waitTime);
    public static event WaitSelectorDelegate WaitTimeChange;

    // Use this for initialization
    void Start () {
        dropdown = GetComponent<Dropdown>();
        OnDropdownChanged(dropdown.value); // send initial message

    }

    public void OnDropdownChanged(int id)  // invoke PatternChange
    {
        double waitTime = double.Parse(dropdown.options[id].text);
        
        if (WaitTimeChange != null)
            WaitTimeChange.Invoke(waitTime);
    }
    
}
