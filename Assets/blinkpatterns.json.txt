{
    "patterns": [
        {
            "genus": "Photinus",
            "species": "marginellus",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0,
                    "duration": 0.33 
                },
                {
                    "startTime": 2.83,
                    "duration": 0.33 
                },
                {
                    "startTime": 5.64,
                    "duration": 0.33
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "marginellus",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 0.46,
                    "duration": 0.23 
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "sabulosus",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0.04,
                    "duration": 0.13 
                },
                {
                    "startTime": 3.79,
                    "duration": 0.13 
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "sabulosus",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 0.44,
                    "duration": 0.15 
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "pyralis",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0,
                    "duration": 0.69 
                },
                {
                    "startTime": 5.61,
                    "duration": 0.69 
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "pyralis",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 2.65,
                    "duration": 0.44
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "umbratus",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0,
                    "duration": 0.51 
                },
                {
                    "startTime": 6.72,
                    "duration": 0.26 
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "umbratus",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 0.83,
                    "duration": 0.73
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "collustrans",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0,
                    "duration": 0.34 
                },
                {
                    "startTime": 2.21,
                    "duration": 0.34
                },
                {
                    "startTime": 4.41,
                    "duration": 0.34
                },
                {
                    "startTime": 6.85,
                    "duration": 0.15
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "collustrans",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 1.81,
                    "duration": 0.92
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "ignitus",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0,
                    "duration": 0.25 
                },
                {
                    "startTime": 5,
                    "duration": 0.25
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "ignitus",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 3.98,
                    "duration": 0.62
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "consanguineus",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0,
                    "duration": 0.15 
                },
                {
                    "startTime": 0.5,
                    "duration": 0.17
                },
                {
                    "startTime": 5.31,
                    "duration": 0.17
                },
                {
                    "startTime": 5.77,
                    "duration": 0.17
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "consanguineus",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 1.23,
                    "duration": 0.48
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "greeni",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0.04,
                    "duration": 0.11 
                },
                {
                    "startTime": 1.31,
                    "duration": 0.11
                },
                {
                    "startTime": 5.0,
                    "duration": 0.11
                },
                {
                    "startTime": 6.29,
                    "duration": 0.11
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "greeni",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 1.23,
                    "duration": 0.48
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "macdermotti",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0.04,
                    "duration": 0.14 
                },
                {
                    "startTime": 1.98,
                    "duration": 0.14
                },
                {
                    "startTime": 6.0,
                    "duration": 0.14
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "macdermotti",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 1.23,
                    "duration": 0.42
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "consimilis",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0,
                    "duration": 0.07 
                },
                {
                    "startTime": 0.3,
                    "duration": 0.07
                },
                {
                    "startTime": 0.59,
                    "duration": 0.07
                },
                {
                    "startTime": 0.87,
                    "duration": 0.07
                },
                {
                    "startTime": 1.14,
                    "duration": 0.07
                },
                {
                    "startTime": 1.44,
                    "duration": 0.07
                },
                {
                    "startTime": 1.73,
                    "duration": 0.07
                },
                {
                    "startTime": 2.02,
                    "duration": 0.07
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "consimilis",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 8.01,
                    "duration": 0.27
                },
		{
                    "startTime": 8.52,
                    "duration": 0.27
                },
		{
                    "startTime": 9.0,
                    "duration": 0.27
                },
		{
                    "startTime": 9.58,
                    "duration": 0.27
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "carolinus",
            "sex": "male",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 7,
            "blinks": [
                {
                    "startTime": 0.04,
                    "duration": 0.19 
                },
                {
                    "startTime": 0.57,
                    "duration": 0.19
                },
                {
                    "startTime": 1.14,
                    "duration": 0.19
                },
                {
                    "startTime": 1.71,
                    "duration": 0.19
                },
                {
                    "startTime": 2.28,
                    "duration": 0.19
                },
                {
                    "startTime": 2.83,
                    "duration": 0.19
                }
            ]
        },
        {
            "genus": "Photinus",
            "species": "carolinus",
            "sex": "female",
            "color": {
                "r": 0.0,
                "g": 0.0,
                "b": 0.0,
                "a": 0.0
            },
            "timelineLength": 10,
            "blinks": [
                {
                    "startTime": 7.58,
                    "duration": 0.27
                },
		{
                    "startTime": 7.9,
                    "duration": 0.27
                },
		{
                    "startTime": 8.75,
                    "duration": 0.27
                },
		{
                    "startTime": 9.1,
                    "duration": 0.27
                }
            ]
        }
    ]
}
