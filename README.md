### About Firefly Beacon ###

Firefly Beacon is designed to attract firefly insects to your phone or tablet.  Your screen will flash in the particular sequence of a selected species.  The pattern can be previewed on a timeline before entering full screen mode for general use.  Firefly Beacon is free to use and open source.  

[![Get it on Google Play](https://play.google.com/intl/en_us/badges/images/badge_new.png)](https://play.google.com/store/apps/details?id=com.ksimmulator.FireflyBeacon&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1)

### Contributors ###

Software: Keith Simmons  
Artwork: Leah Brunetto

### Licenses ###

All C# code for this unity app is licensed under GPLv3.  The firefly vectors are licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

### Learn More About Fireflies ###

The patterns used by this application were deciphered from the research of Sara Lewis, PhD and Christopher Cratsley, PhD.  The charts referenced may be found in the following paper: [*Lewis SM, Cratsley CK. (2008) Flash Signal Evolution, Mate Choice, and Predation in Fireflies.  Annual Review of Entomology 53, 293-321*](http://ase.tufts.edu/biology/labs/lewis/publications/documents/2008Lewis_Cratsley.pdf)

### Screenshots ###

![27886419285_f7f2ccfe41_z.jpg](https://bitbucket.org/repo/bL54Ej/images/1224645546-27886419285_f7f2ccfe41_z.jpg)
![27790002992_2553a3e629_z.jpg](https://bitbucket.org/repo/bL54Ej/images/1154484825-27790002992_2553a3e629_z.jpg)
![27858276896_9a73d62310_z.jpg](https://bitbucket.org/repo/bL54Ej/images/4252716701-27858276896_9a73d62310_z.jpg)
![27858302756_125fb08a7b_z.jpg](https://bitbucket.org/repo/bL54Ej/images/272402935-27858302756_125fb08a7b_z.jpg)